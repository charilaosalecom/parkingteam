package com.chlaliotis.parkschallenge
import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import com.chlaliotis.parkschallenge.database.ParksTeamDataBase
import com.chlaliotis.parkschallenge.extensions.convertISOTimeToDate
import com.chlaliotis.parkschallenge.model.ParksTeam
import com.chlaliotis.parkschallenge.repository.ParksTeamRepository
import com.chlaliotis.parkschallenge.repository.ParksTeamRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.util.*
import java.io.IOException as IOException1

class App : Application() {

    companion object {
        private lateinit var instance: App

        private val database: ParksTeamDataBase by lazy {
            ParksTeamDataBase.buildDatabase(instance)
        }

        val repository: ParksTeamRepository by lazy {
            ParksTeamRepositoryImpl(
                database.parksTeamDao()

            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        instance = this
        parseCsv()


    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun parseCsv(){
        val filetoparse = resources.openRawResource(R.raw.parks)
        val inputStream: BufferedReader = filetoparse.bufferedReader()



        var fileReader: BufferedReader? = null
        val parksteam = ArrayList<ParksTeam>()

        try {

            var line: String?

            fileReader = inputStream

            // Read CSV header
            fileReader.readLine()

            // Read the file line by line starting from the second line
            line = fileReader.readLine()
            while (line != null) {
                val tokens = line.split("\t")
                if (tokens.size > 0) {

                    val list: List<String> = tokens[1].split("\t".toRegex())
                    val endparktime = list[0]
                    val camid = tokens[2].substringAfter(" ").split("\t")[0].replace("\\s".toRegex(), "")
                    val parksTeam = ParksTeam(
                        id= tokens[0].toInt(),

                        startArea =  convertISOTimeToDate(tokens[1])!!,
                        endArea = convertISOTimeToDate(endparktime)!!,
                        camid = camid,
                        plate = tokens[3].toString(),
                        startPark = convertISOTimeToDate(tokens[4])!!,
                        endPark = convertISOTimeToDate(tokens[5])!!,

                        )
                    parksteam.add(parksTeam)
                }

                line = fileReader.readLine()
            }

            // Print the new customer list
            for (xs in parksteam) {
                println(xs)
            }
        } catch (e: Exception) {
            println("Reading CSV Error!")
            e.printStackTrace()
        } finally {
            try {
                fileReader!!.close()
            } catch (e: IOException1) {
                println("Closing fileReader Error!")
                e.printStackTrace()
            }
        }
        if(repository.getParksTeam().isNullOrEmpty()){
            parksteam.forEach{item->
                repository.addParksTeam(item)
            }
        }


    }



}
