package com.chlaliotis.parkschallenge

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

class MainActivity : AppCompatActivity() {

    private val repository by lazy{App.repository}

    @ExperimentalTime
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        print("Hello")
        val allrecords = repository.getParksTeam()
        val hourformat = SimpleDateFormat("HH", Locale.US)
        val minuteformat = SimpleDateFormat("mm", Locale.US)



        val allrecordasCallendar = allrecords.filter {
            hourformat.format(it.startArea).toInt() >=9 && hourformat.format(it.endArea).toInt() <=12 }

         val allcarsneedtopay = allrecords.filter {(it.endPark.time/(1000*60) -it.startPark.time/(1000*60)) > 20 }



        val carsinAreatSlot9 = 10 - allrecords.filter {
            hourformat.format(it.startPark).toInt() >=9 && hourformat.format(it.endPark).toInt() <10 }.size

        val carsinAreatSlot10 = 10 - allrecords.filter {
            hourformat.format(it.startPark).toInt() >=10 && hourformat.format(it.endPark).toInt() <11 }.size
        val carsinAreatSlot11 = 10 - allrecords.filter {
            hourformat.format(it.startPark).toInt() >=11 && hourformat.format(it.endPark).toInt() <12 }.size

        val carsinAreatSlot12 = 10 - allrecords.filter {
            hourformat.format(it.startPark).toInt() >=12&& hourformat.format(it.endPark).toInt() <13 }.size

        val textResults1 = findViewById<TextView>(R.id.textViewResults1)
        val texsResulst2 = findViewById<TextView>(R.id.textViewResults2)
        textResults1.apply {
            setText("Between 9 and 12 there are " + allrecordasCallendar.size + " cars")
        }
        texsResulst2.apply {
            setText("${allcarsneedtopay.size}  cars need to pay and the freed slots for 9,10,11,12 is ${carsinAreatSlot9}, ${carsinAreatSlot10}, ${carsinAreatSlot11}, ${carsinAreatSlot12} ")
        }

        val linechart1 = findViewById<LineChart>(R.id.lineChart1)
        val linechart2 = findViewById<LineChart>(R.id.lineChart2)
        val linechart3 = findViewById<LineChart>(R.id.lineChart3)
        val entries = ArrayList<Entry>()

        val labels = mutableListOf<String>()
        labels.add("9:00")
        val carsinslot0 =  allrecords.filter {
            hourformat.format(it.startPark).toInt() >=9 && hourformat.format(it.endPark).toInt() <=10 && minuteformat.format(it.endPark).toInt()<30 }.size
        labels.add("9:30")
        val carsinslot1 =  allrecords.filter {
            hourformat.format(it.startPark).toInt() >=9 && hourformat.format(it.endPark).toInt() <=9 && minuteformat.format(it.startPark).toInt() >30 }.size

        labels.add("10:00")
        val carsinlot2 = allrecords.filter {
            hourformat.format(it.startPark).toInt() >=10 && hourformat.format(it.endPark).toInt() <=11 && minuteformat.format(it.endPark).toInt()<30 }.size
        labels.add("10:30")

        val carsinlot3 = allrecords.filter {
            hourformat.format(it.startPark).toInt() >=10 && hourformat.format(it.endPark).toInt() <=11 && minuteformat.format(it.endPark).toInt()>30 }.size

        labels.add("11:00")
        val carsinlot4 = allrecords.filter {
            hourformat.format(it.startPark).toInt() >=11 && hourformat.format(it.endPark).toInt() <=12 && minuteformat.format(it.endPark).toInt()<30 }.size
        labels.add("11:30")
        val carsinlot5= allrecords.filter {
            hourformat.format(it.startPark).toInt() >=11 && hourformat.format(it.endPark).toInt() <=12 && minuteformat.format(it.endPark).toInt()>30 }.size
        labels.add("12:00")

        val xAxis = linechart1.getXAxis()
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.labelCount = labels.size // important
        xAxis.setSpaceMax(0.5f) // optional
        xAxis.setSpaceMin(0.5f) // optional
        xAxis.valueFormatter = object : ValueFormatter() {
            override
            fun getFormattedValue(value: Float): String {
                // value is x as index
                return labels[value.toInt()]
            }
        }
        val entriesb = ArrayList<Entry>()
        val entriesc = ArrayList<Entry>()


        val labelsb = mutableListOf<String>()
        labelsb.add("9:00")
        val carsinslot0b =  allrecords.filter {
            hourformat.format(it.startArea).toInt() >=9 && hourformat.format(it.endArea).toInt() <=10 && minuteformat.format(it.endArea).toInt()<30 }.size
        labelsb.add("9:30")
        val carsinslot1b =  allrecords.filter {
            hourformat.format(it.startArea).toInt() >=9 && hourformat.format(it.endArea).toInt() <=9 && minuteformat.format(it.endArea).toInt() >30 }.size

        labelsb.add("10:00")
        val carsinlot2b = allrecords.filter {
            hourformat.format(it.startArea).toInt() >=10 && hourformat.format(it.endArea).toInt() <=11 && minuteformat.format(it.endArea).toInt()<30 }.size
        labelsb.add("10:30")

        val carsinlot3b = allrecords.filter {
            hourformat.format(it.startArea).toInt() >=10 && hourformat.format(it.endArea).toInt() <=11 && minuteformat.format(it.endArea).toInt()>30 }.size

        labelsb.add("11:00")
        val carsinlot4b = allrecords.filter {
            hourformat.format(it.startArea).toInt() >=11 && hourformat.format(it.endArea).toInt() <=12 && minuteformat.format(it.endArea).toInt()<30 }.size
        labelsb.add("11:30")
        val carsinlot5b= allrecords.filter {
            hourformat.format(it.startArea).toInt() >=11 && hourformat.format(it.endArea).toInt() <=12 && minuteformat.format(it.endArea).toInt()>30 }.size
        labelsb.add("12:00")

        val xAxisb = linechart2.getXAxis()
        xAxisb.setPosition(XAxis.XAxisPosition.TOP);
        xAxisb.setDrawAxisLine(true);
        xAxisb.setDrawGridLines(false);
        xAxisb.setDrawLabels(true);
        xAxisb.labelCount = labels.size // important
        xAxisb.setSpaceMax(0.5f) // optional
        xAxisb.setSpaceMin(0.5f) // optional
        xAxisb.valueFormatter = object : ValueFormatter() {
            override
            fun getFormattedValue(value: Float): String {
                // value is x as index
                return labels[value.toInt()]
            }
        }




//Part2

        entries.add(Entry(0f, carsinslot0.toFloat()))
        entries.add(Entry(1f, carsinslot1.toFloat()))
        entries.add(Entry(2f,carsinlot2.toFloat() ))
        entries.add(Entry(3f,carsinlot3.toFloat() ))
        entries.add(Entry(4f,carsinlot4.toFloat() ))
        entries.add(Entry(5f,carsinlot5.toFloat() ))



//Part3
        val vl = LineDataSet(entries, "Cars need to pay parking")

//Part4
        vl.setDrawValues(false)
        vl.setDrawFilled(true)
        vl.lineWidth = 3f
        vl.fillColor = Color.GRAY
        vl.fillAlpha = Color.RED

//Part5
        linechart2.xAxis.labelRotationAngle = 0f

//Part6
        linechart1.data = LineData(vl)
        xAxisb.setPosition(XAxis.XAxisPosition.TOP);
        xAxisb.setDrawAxisLine(true);
        xAxisb.setDrawGridLines(false);
        xAxisb.setDrawLabels(true);
        xAxisb.labelCount = labels.size // important
        xAxisb.setSpaceMax(0.5f) // optional
        xAxisb.setSpaceMin(0.5f) // optional
        xAxisb.valueFormatter = object : ValueFormatter() {
            override
            fun getFormattedValue(value: Float): String {
                // value is x as index
                return labels[value.toInt()]
            }
        }




//Part2

        entriesb.add(Entry(0f, carsinslot0b.toFloat()))
        entriesb.add(Entry(1f, carsinslot1b.toFloat()))
        entriesb.add(Entry(2f,carsinlot2b.toFloat() ))
        entriesb.add(Entry(3f,carsinlot3b.toFloat() ))
        entriesb.add(Entry(4f,carsinlot4b.toFloat() ))
        entriesb.add(Entry(5f,carsinlot5b.toFloat() ))



//Part3
        val vlb = LineDataSet(entriesb, "Cars to stay in the parking")

//Part4
        vlb.setDrawValues(false)
        vlb.setDrawFilled(true)
        vlb.lineWidth = 3f
        vlb.fillColor = Color.GRAY
        vlb.fillAlpha = Color.RED

//Part5
        linechart2.xAxis.labelRotationAngle = 0f

//Part6
        linechart1.data = LineData(vl)

        linechart2.data = LineData(vlb)

//Part7
        linechart2.axisRight.isEnabled = false

//Part8
        linechart2.setTouchEnabled(true)
        linechart2.setPinchZoom(true)

//Part9
        linechart2.description.text = "Cars in the area"
        linechart2.description.text = "Cars in the area that is reuuired to pay"
        linechart2.setNoDataText("No forex yet!")


//Part10
        linechart1.animateX(1800, Easing.EaseInExpo)


        val labelsc = mutableListOf<String>()
        labelsc.add("9:00")
        val cartopayinlot0 =  allcarsneedtopay.filter {
            hourformat.format(it.startPark).toInt() >=9 && hourformat.format(it.endPark).toInt() <=10 }.size * 1.5
        labelsc.add("10:00")
        val cartopayinslot1 =  allcarsneedtopay.filter {
            hourformat.format(it.startPark).toInt() >=10 && hourformat.format(it.endPark).toInt() <=11 }.size * 1.5
        labelsc.add("11:00")
        val cartopayinslot2 =  allcarsneedtopay.filter {
            hourformat.format(it.startPark).toInt() >=11 && hourformat.format(it.endPark).toInt() <=12 }.size * 1.5
        val xAxisc = linechart3.getXAxis()
        xAxisc.setPosition(XAxis.XAxisPosition.TOP);
        xAxisc.setDrawAxisLine(true);
        xAxisc.setDrawGridLines(false);
        xAxisc.setDrawLabels(true);
        xAxisc.labelCount = labelsc.size // important
        xAxisc.setSpaceMax(0.5f) // optional
        xAxisc.setSpaceMin(0.5f) // optional
        xAxisc.valueFormatter = object : ValueFormatter() {
            override
            fun getFormattedValue(value: Float): String {
                // value is x as index
                return labelsc[value.toInt()]
            }
        }

        entriesc.add(Entry(0f, cartopayinlot0.toFloat()))
        entriesc.add(Entry(1f,cartopayinslot1.toFloat()))
        entriesc.add(Entry(2f,cartopayinslot2.toFloat()))



        val vlc = LineDataSet(entriesc, "Revenue expected")

//Part4
        vlc.setDrawValues(false)
        vlc.setDrawFilled(true)
        vlc.lineWidth = 3f
        vlc.fillColor = Color.GRAY
        vlc.fillAlpha = Color.RED

//Part5
        linechart3.xAxis.labelRotationAngle = 0f

//Part6
        linechart3.data = LineData(vlc)
        linechart2.description.text = "Expected Revenue"









    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


}