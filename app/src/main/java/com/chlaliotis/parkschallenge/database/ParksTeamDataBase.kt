package com.chlaliotis.parkschallenge.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.chlaliotis.parkschallenge.database.converter.DateTimeConverter
import com.chlaliotis.parkschallenge.database.dao.ParksTeamDao
import com.chlaliotis.parkschallenge.model.ParksTeam

const val DATABASE_VERSION = 1


@Database(
    entities = [ParksTeam::class],
    version = DATABASE_VERSION
)
@TypeConverters(DateTimeConverter::class)
abstract class ParksTeamDataBase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "ParksTeam"

        fun buildDatabase(context: Context): ParksTeamDataBase {
            return Room.databaseBuilder(
                context,
                ParksTeamDataBase::class.java,
                DATABASE_NAME
            ).allowMainThreadQueries()
                .build()
        }
    }

    abstract fun parksTeamDao(): ParksTeamDao


}