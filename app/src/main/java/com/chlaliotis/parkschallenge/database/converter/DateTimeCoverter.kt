package com.chlaliotis.parkschallenge.database.converter
import androidx.room.TypeConverter
import java.util.*

class DateTimeConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return Date(value ?: 0)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long = date?.time ?: 0
}