package com.chlaliotis.parkschallenge.database.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chlaliotis.parkschallenge.model.ParksTeam


@Dao
interface ParksTeamDao {

    @Query("SELECT * FROM ParksTeam")
     fun getParksTeam(): List<ParksTeam>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addParksTeam(parksTeam: ParksTeam)
}