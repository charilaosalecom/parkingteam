package com.chlaliotis.parkschallenge.extensions

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun convertISOTimeToDate(isoTime: String): Date? {
    val sdf = SimpleDateFormat("MM/dd/yyyy'-'HH.mm")
    var convertedDate: Date? = null
    var formattedDate: String? = null
    try {
        convertedDate = sdf.parse(isoTime)

    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return convertedDate
}