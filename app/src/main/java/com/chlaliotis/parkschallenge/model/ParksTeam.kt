package com.chlaliotis.parkschallenge.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.chlaliotis.parkschallenge.database.converter.DateTimeConverter
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity
data class ParksTeam(
    @PrimaryKey
    val id: Int ,
    @TypeConverters(DateTimeConverter::class)
    val startArea: Date,

    @TypeConverters(DateTimeConverter::class)
    val endArea: Date,

    val plate: String,

    val camid:String,

    @TypeConverters(DateTimeConverter::class)
    val startPark: Date,


    @TypeConverters(DateTimeConverter::class)
    val endPark: Date,


    ) : Parcelable