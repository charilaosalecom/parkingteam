package com.chlaliotis.parkschallenge.repository

import com.chlaliotis.parkschallenge.model.ParksTeam


    interface ParksTeamRepository {

         fun addParksTeam(parksTeam: ParksTeam)

         fun getParksTeam(): List<ParksTeam>
    }
