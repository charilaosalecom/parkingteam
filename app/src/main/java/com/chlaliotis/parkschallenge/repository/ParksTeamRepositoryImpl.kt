package com.chlaliotis.parkschallenge.repository

import com.chlaliotis.parkschallenge.database.dao.ParksTeamDao
import com.chlaliotis.parkschallenge.model.ParksTeam

class ParksTeamRepositoryImpl(private val parksTeamDao: ParksTeamDao,) : ParksTeamRepository {
    override  fun addParksTeam(parksTeam: ParksTeam) = parksTeamDao.addParksTeam(parksTeam)
    override  fun getParksTeam(): List<ParksTeam> = parksTeamDao.getParksTeam()
}